#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

/* the idea: take the first input, if the next input equal to the first input, counter +1
            take the next input, if still equall +1 untill are not equal and print the final counter + the word
            else print 1 and the first input then compare the future input with current one.
                repeat steps on 1.
*/
void ShowCount()
{
    string currentInput;
    string previousInput;


        int counter=1;

        getline(cin,previousInput);
        cout<<counter<<" "<<previousInput<<endl;
        while(getline(cin,currentInput))
        {
            if(currentInput==previousInput)
            {
                counter++;
                cout<<counter<<" "<<currentInput<<endl;
                previousInput=currentInput;
            }
            else if(currentInput!=previousInput)
            {
                counter=1;
                cout<<counter<<" "<<currentInput<<endl;
                previousInput=currentInput;
            }
        }
}

void Duplicated() //uniq -d work as intended
{
    string currentInput;
    string previousInput;

    int counter=1;

    getline(cin,previousInput);

    while(getline(cin,currentInput))
    {
        if(currentInput!=previousInput)
        {
            previousInput=currentInput;
            getline(cin,currentInput);
        }

        while(currentInput==previousInput)
        {
        counter++;
        previousInput=currentInput;
        getline(cin,currentInput);
        if(previousInput!=currentInput &&counter>1)
           {
            cout<<"Duplicated"<<previousInput<<endl;
            counter=1;
           }
        }
        previousInput=currentInput;
    }
}
void PrintUniq()
{
    string currentInput;
    string previousInput;

    int counter=1;
    getline(cin,previousInput);

    while( getline(cin,currentInput))
    {
        if(previousInput==currentInput)
        {
            counter++;
            previousInput=currentInput;
            getline(cin,currentInput);

             if(counter>1)
            {
                counter=1;
                previousInput=currentInput;
                getline(cin,currentInput);
            }
        }
        else if(previousInput != currentInput && counter ==1)
        {
            cout<<"uniq "<<previousInput;
            previousInput=currentInput;
        }

    }

}


int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				ShowCount();
				break;
			case 'd':
				dupsonly = 1;
				Duplicated();
				break;
			case 'u':
				uniqonly = 1;
				PrintUniq();
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */


	return 0;
}
